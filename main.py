import http.client
import csv
from math import floor
import time

class CommuneFile:
    def __init__(self, filename):
        self.filename = filename
        
    def readData(self):
        communeList = []    
        with open(self.filename, 'r') as InputCSV:
            reader=csv.reader(InputCSV, delimiter=',')
            next(reader)
            for row in reader:
                name = row[3]
                lat = row[15]
                lon = row[16]
                codeINSEE = row[4]
                communeList.append(Commune(name, lat, lon, codeINSEE))
        return communeList
        
class Commune():
    def __init__(self, name, lat, lon, codeINSEE):
        self.name = name
        self.lat = lat
        self.lon = lon
        self.codeINSEE = codeINSEE

def addPart(dataList, boundary, name, value):
    newLine = '\r\n'
    dataList.append('--' + boundary + newLine)
    dataList.append('Content-Disposition: form-data; name="{0}"'.format(name) + newLine + newLine)
    dataList.append('{0}'.format(value) + newLine)

class PVGisData():
    def getData(self, lat,lon):
        boundary = 'wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T' # Randomly generated
        dataList = []
        addPart(dataList, boundary, 'MAX_FILE_SIZE', '10000')
        addPart(dataList, boundary, 'pv_database', 'PVGIS-CMSAF')
        addPart(dataList, boundary, 'pvtechchoice', 'crystSi')
        addPart(dataList, boundary, 'peakpower', '1')
        addPart(dataList, boundary, 'efficiency', '14')
        addPart(dataList, boundary, 'mountingplace', 'free')
        addPart(dataList, boundary, 'optimalangles', 'true')
        addPart(dataList, boundary, 'horizonfile', '')
        addPart(dataList, boundary, 'outputchoicebuttons', 'csv')
        addPart(dataList, boundary, 'sbutton', 'Calculate')
        addPart(dataList, boundary, 'outputformatchoice', 'csv')
        addPart(dataList, boundary, 'optimalchoice', '')
        #addPart(dataList, boundary, 'angle', '30')
        #addPart(dataList, boundary, 'aspectangle', '180')
        addPart(dataList, boundary, 'latitude', lat)    
        addPart(dataList, boundary, 'longitude', lon)       
        addPart(dataList, boundary, 'regionname', 'africa')
        addPart(dataList, boundary, 'language', 'en_en')   
        headers = {'Content-type': 'multipart/form-data; boundary={0}'.format(boundary)}    
        body = ''.join(dataList)    
        conn = http.client.HTTPConnection('re.jrc.ec.europa.eu')
        conn.request('POST', '//pvgis/apps4/PVcalc.php', body, headers)   
        response = conn.getresponse()        
        self.payload = response.read().decode('iso-8859-1') # This is the full reply of the server
        self.table = self.payload.split('\n')[8:20]
    
    def em(self):    
        return self.getTot(4)
    
    def hm(self):
        return self.getTot(8)    
        
    def getTot(self, nCol):                
        tot=0.0
        for row in self.table :                  # wander through the lines of the array        
            cols = row.split('\t')       # split the row into colums (seperator: tab)        
            tot = tot + float(cols[nCol])        # add up the total value#                
        return tot  
    
    def inclination(self):
        return float(self.payload.split('\n')[3].split(':')[1].replace('deg.',''))
    
    def azimuth(self):
        return float(self.payload.split('\n')[4].split(':')[1].replace('deg.',''))

def convertDegree(Deg):
    degree=floor(Deg)
    minute=Deg-degree    
    minute_=minute*60    
    minute=floor(minute_)    
    second=minute_-minute
    second=second*60
    second=floor(second)    
    degree=str(int(degree))
    minute=str(int(minute))
    second=str(int(second))           
    out= degree+u"\u00B0"+minute+u"\u0027" + second+'"'        
    return out

inFile='PV_GIS_XY_DOMTOM/XYCom976.csv'
myCommuneFile = CommuneFile(inFile)
communeList = myCommuneFile.readData()

with open('out.csv','w') as outFile:
    csvwriter = csv.writer(outFile, delimiter=';', lineterminator='\n')
    csvwriter.writerow(['INSEE Code', 'Name', 'lat', 'lon', 'totEM', 'totHM', 'inclination', 'azimuth'])
    pvgis = PVGisData()       
    for commune in communeList:
        time.sleep(1.4)
        print(commune.name)                
        pvgis.getData(commune.lat, commune.lon)            
        csvwriter.writerow([commune.codeINSEE, commune.name, commune.lat, commune.lon, pvgis.em(), pvgis.hm(), pvgis.inclination(), pvgis.azimuth()])
        
        